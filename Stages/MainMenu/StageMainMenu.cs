// Stage menu: simple menu script connecting local and networked game signals to our scene manager.
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class StageMainMenu : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.MainMenuSound, AudioStream> _mainMenuSounds = AudioData.LoadedSounds<AudioData.MainMenuSound>(AudioData.MainMenuSoundPaths);
	private PictureStory _pictureStory;
	private Panel _popAbout;
	private PnlSettings _pnlSettings;

	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);

	public override void _Ready()
	{
		base._Ready();
		// SetBtnToggleFullScreenText();
		GetNode<ScoreManagerMono>("ScoreManagerMono").RefreshLeaderboard(10);
		_popAbout = GetNode<Panel>("PopAbout");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_pictureStory = GetNode<PictureStory>("PictureStory");
		_pnlSettings = GetNode<PnlSettings>("PnlSettings");
		_pnlSettings.Visible = false;

		if (OS.HasFeature("HTML5"))
		{
			GetNode<Button>("HBoxContainer/BtnQuit1").Visible = false;
			GetNode<Button>("HBoxContainer/BtnToggleMute").Visible = true;
			_pnlSettings.GetNode<CheckBox>("CntPanels/PnlGraphics/CBoxFullScreen").Visible = false;
			_pnlSettings.GetNode<Label>("CntPanels/PnlGraphics/LblTitle").Text = "Not available in HTML5 version.";
		}
		
		AudioHandler.PlaySound(_musicPlayer, _mainMenuSounds[AudioData.MainMenuSound.Music], AudioData.SoundBus.Music);

		
		_popAbout.Visible = false;
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master")) ? "UNMUTE" : "MUTE";
		
		_musicPlayer.PitchScale = 1f;
		int busEffectsIndex = AudioServer.GetBusIndex(AudioData.SoundBusStrings[AudioData.SoundBus.Music]);
		int numberofEffects = AudioServer.GetBusEffectCount(busEffectsIndex);
		for (int i = 0; i < numberofEffects; i++)
		{
			AudioServer.RemoveBusEffect(busEffectsIndex, i);
		}
	}

	private void OnBtnPlayPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
	}

	private void OnBtnAboutPressed()
	{
		GetNode<Panel>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = true;
	}

	private void OnBtnBackPressed()
	{
		GetNode<Panel>("PopAbout").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		if (_popAbout.Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > _popAbout.RectGlobalPosition.x && evMouseButton.Position.x < _popAbout.RectSize.x + _popAbout.RectGlobalPosition.x
			&& evMouseButton.Position.y > _popAbout.RectGlobalPosition.y && evMouseButton.Position.y < _popAbout.RectSize.y + _popAbout.RectGlobalPosition.y) )
			{
				OnBtnBackPressed();
			}
		}
	}

	private void _on_BtnQuit_pressed()
	{
		GetTree().Quit();
	}

	private void ToggleMute()
	{
		bool muted = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master"));
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Master"), !muted);
		string label = muted ? "MUTE" : "UNMUTE";
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = label;
	}

	private void OnGameLinkButtonPressed()
	{
		OS.ShellOpen("https://sage7.itch.io/");
	}


	private void OnBtnScoresPressed()
	{
		_pnlSettings.Visible = true;
	}

	private void OnBtnTexBackgroundPressed()
	{
		_pnlSettings.OnBtnClosePressed();
		_popAbout.Visible = false;
	}

	private void OnBtnLeaderboardPressed()
	{
		GetNode<ScoreManagerMono>("ScoreManagerMono").ShowLeaderboard();
	}

}


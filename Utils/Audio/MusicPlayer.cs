using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class MusicPlayer : AudioStreamPlayer
{
	private Tween _musicTween;
	private float _startingVolumeDb;

	[Export]
	private float _fadeDuration = 1f;

	private Random _rand = new Random();

	private List<AudioStream> _currentPlaylist;

	private List<AudioStream> _finishedPlaylist = new List<AudioStream>();

	private bool _playlistActive = false;

	private bool _stop = false;
	// private bool _looping = false;

	public Dictionary<AudioStream, float> PausedMusic = new Dictionary<AudioStream, float>();

	public override void _Ready()
	{
		_startingVolumeDb = VolumeDb;
		Tween t = new Tween();
		AddChild(t);
		_musicTween = t;
		// VolumeDb = -80;
	}

	public override void _Process(float delta)
	{
		if (Stream == null)
		{
			return;
		}
		float timeLeft = Stream.GetLength() - GetPlaybackPosition();
		if (timeLeft <= _fadeDuration && timeLeft > 0.1f && !_musicTween.IsActive())
		{
			FadeOut();
		}
	}

	// this works without playlist - if you want to manually control music being played e.g. based on entering an area
	public async void PauseAndPlayNext(AudioStream newStream)
	{
		
		if (Stream != null)
		{
			if (PausedMusic.ContainsKey(Stream))
			{
				PausedMusic.Remove(Stream);
			}
			PausedMusic[Stream] = GetPlaybackPosition();
		}
		
		FadeOut();
		await ToSignal(_musicTween, "tween_completed");
		Stop();
		Stream = newStream;		
		if (PausedMusic.ContainsKey(newStream))
		{
			FadeIn();
			this.Play(PausedMusic[newStream]);
		}
		else
		{
			// VolumeDb = _startingVolumeDb;
			this.Play();
		}
	}

	public void FadeIn()
	{
		_musicTween.InterpolateProperty(this, "volume_db", -80, _startingVolumeDb, _fadeDuration, Tween.TransitionType.Linear, Tween.EaseType.Out);
		_musicTween.Start();
	}

	public void FadeOut(float volDB = -80)
	{
		_musicTween.InterpolateProperty(this, "volume_db", _startingVolumeDb, volDB, _fadeDuration, Tween.TransitionType.Linear, Tween.EaseType.In);
		_musicTween.Start();
		// GD.Print("fading out current stream");
	}

	public async void FadeThenStop()
	{
		_stop = true;
		_playlistActive = false;
		FadeOut();
		await ToSignal(_musicTween, "tween_completed");
		Stop();
	}


	public new void Play(float from = 0)
	{
		_stop = false;
		_musicTween.StopAll();
		if (from == 0)
		{
			VolumeDb = _startingVolumeDb;
		}
		// _musicTween.StopAll();
		// _musicTween.InterpolateProperty(this, "volume_db", -80, _startingVolumeDb, _fadeDuration, Tween.TransitionType.Linear, Tween.EaseType.Out);
		// _musicTween.Start();
		base.Play(from);
	}

	public void StartPlaylist(List<AudioStream> streams)
	{
		_playlistActive = true;
		_currentPlaylist = streams;
		PlayNext();
	}

	private void PlayNext()
	{
		// GD.Print("playing next stream");
		Stream = _currentPlaylist[_rand.Next(0,_currentPlaylist.Count)];

		this.Bus = AudioData.SoundBusStrings[AudioData.SoundBus.Music];
		Play();

		_finishedPlaylist.Add(Stream);
		_currentPlaylist.Remove(Stream);

		if (_currentPlaylist.Count == 0)
		{
			_currentPlaylist = _finishedPlaylist.ToList();
			_finishedPlaylist.Clear();
			// GD.Print("resetting playlist");
		}
	}

	public void StopPlaylist()
	{
		_playlistActive = false;
		_finishedPlaylist.Clear();
		_currentPlaylist.Clear();
	}

	private void OnMusicPlayerFinished()
	{
		if (_stop)
		{
			return;
		}
		if (_playlistActive)
		{
			PlayNext();
			return;
		}
		Play();
	}

}

